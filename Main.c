/*
This program is the first test of communication
utilizing the nRF24L01 RF Module.

By: Jack Milkovich
*/


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "Common.h"
#include "SPI.h"
#include "uart.h"
#include "msp.h"
#include "NRF24L01.h"
#include <inttypes.h>

#define CHAR_COUNT 2

/*
	**CSN set low before every instruction
	**To write an instruction:
		- INSTRUCTION WORD <MSBit to LSBit (one byte)>
		- Data bytes <LSByte to MSByte, MSBit in each byte first>
		(ALL REGISTER INFO: PG. 22 - 26)
	**To read a register: address = 0b000XXXXX where X can be 1 or 0
	**To write a register: address = 0b001XXXXX where X can be 1 or 0
	
	steps for Receive setup:
	-(DONE)Use the same CRC config.
	-(DONE)Set PRIM_RX to 1
	-(DONE)Disable auto acknowledgement on data pipe that
	is addressed
	-(DONE)Use the same address width as PTX device
	-(DONE)Use the same frequency channel as PTX device
	-(DONE)Select data rate 1Mbit/s on both TX and RX devices
	-(DONE)Set correct payload width on data pipe that is addressed
	-(DONE)Set PWR_UP and CE High
	
	A byte = 0x01 = 8 bits
	
	
	**CSN set back to high at end of instruction
	*/
int main(void){
	uint8_t address;
	uint8_t RX_STATUS;
	uint64_t initDataLong = 0;
	int character_count = 0;
	char character[CHAR_COUNT+1];
	char temp[CHAR_COUNT+1];
	int result;
	
	//debug uart code
	uart0_init();
	
	//Init SPI stuff
	Init_Recieve_NRF_SPI();

	//Init_Transmit_NRF_SPI();

	NRF_RX_Reg_Config();
	
	//NRF_TX_Reg_Config();
	
	
	
	
	uart0_put("\r\nENTERING RX...\r\n");
	while(1){
		
		//RX CE HIGH
		P3->OUT |= 1;
		
		
		
		//COPY address from TX_ADDR to PIPE 0 in RX_ADDR_P0 reg.
		//set CSN to low
		P2->OUT &= ~8;
		//RX_ADDR_P0
		address = 0x2A;		
		initDataLong = 0x0101010101;	//same addr as TX_ADDR
		SPI_Recieve_transfer(address);
		SPI_Recieve_transfer(0x01);
		SPI_Recieve_transfer(0x01);
		SPI_Recieve_transfer(0x01);
		SPI_Recieve_transfer(0x01);
		SPI_Recieve_transfer(0x01);
		//set CSN to high
		P2->OUT |= 8;
		
		
		
		
		//READ STATUS REG. for RX. RX_P_NO tells what pipe data is in
		uart0_put("Reading... RX STATUS: ");
		read_Recieve_NRF_Register(0x07);
		
		
		//set CSN to low
		P2->OUT &= ~8;
		//0b00AA AAAA
		SPI_Recieve_transfer(0x07);
		RX_STATUS = SPI_Recieve_recieve();
		//set CSN to high
		P2->OUT |= 8;
			
		
		while(RX_STATUS != 0x42){
			//redo bascially everything in the main while loop until there is
			//sufficent data in RX NRF
			//RX CE HIGH
			P3->OUT |= 1;
			
			
			
			//COPY address from TX_ADDR to PIPE 0 in RX_ADDR_P0 reg.
			//set CSN to low
			P2->OUT &= ~8;
			//RX_ADDR_P0
			address = 0x2A;		
			initDataLong = 0x0101010101;	//same addr as TX_ADDR
			SPI_Recieve_transfer(address);
			SPI_Recieve_transfer(0x01);
			SPI_Recieve_transfer(0x01);
			SPI_Recieve_transfer(0x01);
			SPI_Recieve_transfer(0x01);
			SPI_Recieve_transfer(0x01);
			//set CSN to high
			P2->OUT |= 8;
			
			
			
			//READ STATUS REG. for RX. RX_P_NO tells what pipe data is in
			uart0_put("Reading... RX STATUS: ");
			read_Recieve_NRF_Register(0x07);
			
			
			//set CSN to low
			P2->OUT &= ~8;
			//0b00AA AAAA
			SPI_Recieve_transfer(0x07);
			RX_STATUS = SPI_Recieve_recieve();
			//set CSN to high
			P2->OUT |= 8;
			
			clear_Interrupts();
		}
		
		//RX CE LOW
		P3->OUT &= ~1;
		uart0_put("\r\nRX Payload is...\r\n");
		Mid_read_Recieve_NRF_Register(0x61);
		
		RX_STATUS = 0;
		
		
		clear_Interrupts();
		
		delayMs(50);
		
		

		
		
	}
}
