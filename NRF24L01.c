#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "Common.h"
#include "SPI.h"
#include "uart.h"
#include "msp.h"
#include <inttypes.h>
#include "NRF24L01.h"

/***************
Recieve NRF functions
***************/
void read_Recieve_NRF_Register(uint8_t address){
	char string[50];
	uint8_t recieve;
	//READ CONFIG
	//set CSN to low
	P2->OUT &= ~8;
	//Do a R_REGISTER instruction
	//0b00AA AAAA
	SPI_Recieve_transfer(address);
	recieve = SPI_Recieve_recieve();
	sprintf(string, "STATUS: 0x%x\r\n", (unsigned int)recieve);
	//sprintf(string, "%" PRIu64 "\r\n", recieve);
	uart0_put(string);
	
	//dummy address for padding
	SPI_Recieve_transfer(0x00);
	//now read the MISO pin (1.7)
	recieve = SPI_Recieve_recieve();
	sprintf(string, "DATA: 0x%x\r\n", (unsigned int)recieve);
	//sprintf(string, "%" PRIu64 "\r\n", recieve);
	uart0_put(string);
	//set CSN to high
	P2->OUT |= 8;
}

void Mid_read_Recieve_NRF_Register(uint8_t address){
	char string[50];
	uint8_t recieve;
	//READ CONFIG
	//set CSN to low
	P2->OUT &= ~8;
	//Do a R_REGISTER instruction
	//0b00AA AAAA
	SPI_Recieve_transfer(address);
	recieve = SPI_Recieve_recieve();
	sprintf(string, "STATUS: 0x%x\r\n", (unsigned int)recieve);
	//sprintf(string, "%" PRIu64 "\r\n", recieve);
	uart0_put(string);
	
	//dummy address for padding
	SPI_Recieve_transfer(0x00);
	//now read the MISO pin (1.7)
	recieve = SPI_Recieve_recieve();
	sprintf(string, "DATA: 0x%x\r\n", (unsigned int)recieve);
	//sprintf(string, "%" PRIu64 "\r\n", recieve);
	uart0_put(string);
	
	//dummy address for padding
	SPI_Recieve_transfer(0x00);
	//now read the MISO pin (1.7)
	recieve = SPI_Recieve_recieve();
	sprintf(string, "DATA: 0x%x\r\n", (unsigned int)recieve);
	//sprintf(string, "%" PRIu64 "\r\n", recieve);
	uart0_put(string);
	//set CSN to high
	P2->OUT |= 8;
}

void Long_read_Recieve_NRF_Register(uint8_t address){
	char string[50];
	uint8_t recieve;
	//set CSN to low
	P2->OUT &= ~8;
	//Do a R_REGISTER instruction
	//0b00AA AAAA
	SPI_Recieve_transfer(address);
	recieve = SPI_Recieve_recieve();
	sprintf(string, "STATUS: 0x%x\r\n", (unsigned int)recieve);
	uart0_put(string);
	
	//dummy address for padding
	SPI_Recieve_transfer(0x00);
	//now read the MISO pin (1.7)
	recieve = SPI_Recieve_recieve();
	sprintf(string, "DATA: 0x%x\r\n", (unsigned int)recieve);
	uart0_put(string);
	
	//dummy address for padding
	SPI_Recieve_transfer(0x00);
	//now read the MISO pin (1.7)
	recieve = SPI_Recieve_recieve();
	sprintf(string, "DATA: 0x%x\r\n", (unsigned int)recieve);
	uart0_put(string);
	
	//dummy address for padding
	SPI_Recieve_transfer(0x00);
	//now read the MISO pin (1.7)
	recieve = SPI_Recieve_recieve();
	sprintf(string, "DATA: 0x%x\r\n", (unsigned int)recieve);
	uart0_put(string);
	
	//dummy address for padding
	SPI_Recieve_transfer(0x00);
	//now read the MISO pin (1.7)
	recieve = SPI_Recieve_recieve();
	sprintf(string, "DATA: 0x%x\r\n", (unsigned int)recieve);
	uart0_put(string);
	
	//dummy address for padding
	SPI_Recieve_transfer(0x00);
	//now read the MISO pin (1.7)
	recieve = SPI_Recieve_recieve();
	sprintf(string, "DATA: 0x%x\r\n", (unsigned int)recieve);
	uart0_put(string);
	
	//set CSN to high
	P2->OUT |= 8;
}

void NRF_RX_Reg_Config(void){
	uint8_t address;
	uint8_t initData;
	uint64_t initDataLong;

		/****************************
	RECIEVE REGISTER CONFIG
	****************************/
	uart0_put("\r\nStarting RF Receive Program...\r\n");
	
	//Set CE LOW
	P3->OUT &= ~1;
	
	
	
	//set CSN to low
	P2->OUT &= ~8;
	//CONFIG REG.
	address = 0x20;	
	initData = 0x0B;	//PWR_UP = 1, PRIM_RX = 1, CRC = 0, EN_CRC = 1, interrupts = 0  
	SPI_Recieve_transfer(address);
	SPI_Recieve_transfer(initData);
	//set CSN to high
	P2->OUT |= 8;
	
	//read register
	uart0_put("Reading... CONFIG REG: ");
	read_Recieve_NRF_Register(0x00);
	
	
	
	//set CSN to low
	P2->OUT &= ~8;
	//EN_RXADDR
	address = 0x22;
	initData = 0x03;
	SPI_Recieve_transfer(address);
	SPI_Recieve_transfer(initData);
	//set CSN to high
	P2->OUT |= 8;
	
	//read register
	uart0_put("Reading... EN_RXADDR REG: ");
	read_Recieve_NRF_Register(0x02);
	
	
	
	//set CSN to low
	P2->OUT &= ~8;
	//EN_AA
	address = 0x21;
	initData = 0x03; //disable auto ACK ***
	SPI_Recieve_transfer(address);
	SPI_Recieve_transfer(initData);
	//set CSN to high
	P2->OUT |= 8;
	
	//read register
	uart0_put("Reading... EN_AA REG: ");
	read_Recieve_NRF_Register(0x01);
	
	
	
	//set CSN to low
	P2->OUT &= ~8;
	//RX_PW_P0 and 1
	address = 0x31;	//P0 first
	initData = 0x02; //2 bytes expected (0xBEEF)
	SPI_Recieve_transfer(address);
	SPI_Recieve_transfer(initData);
	//set CSN to high
	P2->OUT |= 8;
	//set CSN to low
	P2->OUT &= ~8;
	address = 0x32;	//P1 second
	SPI_Recieve_transfer(address);
	SPI_Recieve_transfer(initData);
	//set CSN to high
	P2->OUT |= 8;
	
	//read register
	uart0_put("Reading... RX_PW_P0 REG: ");
	read_Recieve_NRF_Register(0x11);
	
	uart0_put("Reading... RX_PW_P1 REG: ");
	read_Recieve_NRF_Register(0x12);
	
	
	
	//set CSN to low
	P2->OUT &= ~8;
	//SETUP_AW
	address = 0x23;
	initData = 0x03;	//5 byte addr width
	SPI_Recieve_transfer(address);
	SPI_Recieve_transfer(initData);
	//set CSN to high
	P2->OUT |= 8;
	
	//read register
	uart0_put("Reading... SETUP_AW REG: ");
	read_Recieve_NRF_Register(0x03);
	
	
	
	//set CSN to low
	P2->OUT &= ~8;
	//RX_ADDR_P0 and P1
	address = 0x2A; //P0 first
	initDataLong = 0x0101010101;
	SPI_Recieve_transfer(address);
	SPI_Recieve_transfer(0x01);
	SPI_Recieve_transfer(0x01);
	SPI_Recieve_transfer(0x01);
	SPI_Recieve_transfer(0x01);
	SPI_Recieve_transfer(0x01);
	//set CSN to high
	P2->OUT |= 8;
	//set CSN to low
	P2->OUT &= ~8;
	address = 0x2B;
	SPI_Recieve_transfer(address);
	SPI_Recieve_transfer(0x01);
	SPI_Recieve_transfer(0x01);
	SPI_Recieve_transfer(0x01);
	SPI_Recieve_transfer(0x01);
	SPI_Recieve_transfer(0x01);
	//set CSN to high
	P2->OUT |= 8;
	
	//read register
	uart0_put("Reading... RX_ADDR_P0 REG: ");
	Long_read_Recieve_NRF_Register(0x0A);
	
	uart0_put("Reading... RX_ADDR_P1 REG: ");
	Long_read_Recieve_NRF_Register(0x0B);
	
	/****************************
	END OF RECIEVE REGISTER CONFIG
	****************************/
}
/*******************
END RECIEVE NRF FUNCTIONS
*******************/





/***************
TRANSMIT NRF functions
***************/
void read_Transmit_NRF_Register(uint8_t address){
	char string[50];
	uint8_t recieve;
	//READ CONFIG
	//set CSN to low
	P4->OUT &= ~2;
	//Do a R_REGISTER instruction
	//0b00AA AAAA
	SPI_Transmit_transfer(address);
	recieve = SPI_Transmit_recieve();
	sprintf(string, "0x%x\r\n", (unsigned int)recieve);
	//sprintf(string, "%" PRIu64 "\r\n", recieve);
	uart0_put(string);
	
	//dummy address for padding
	SPI_Transmit_transfer(0x00);
	//now read the MISO pin (1.7)
	recieve = SPI_Transmit_recieve();
	sprintf(string, "0x%x\r\n", (unsigned int)recieve);
	//sprintf(string, "%" PRIu64 "\r\n", recieve);
	uart0_put(string);
	//set CSN to high
	P4->OUT |= 2;
}

void NRF_TX_Reg_Config(void){
	uint8_t address;
	uint8_t initData;

	/****************************
	TRANSMIT REGISTER CONFIG
	****************************/
	uart0_put("\r\nStarting RF Transmitter Program...\r\n");
	
	//set CE low
	P4->OUT &= ~64;

	//set CSN to low
	P4->OUT &= ~2;
	//CONFIG Reg.
	address = 0x20;		//0b00100000
	initData = 0x0A;	//PWR_UP = 1, PRIM_RX = 0, CRC = 0, EN_CRC = 1, interrupts = 0
	SPI_Transmit_transfer(address);
	SPI_Transmit_transfer(initData);
	//set CSN to high
	P4->OUT |= 2;

	//read register
	uart0_put("Reading... CONFIG: ");
	read_Transmit_NRF_Register(0x00);
	
	
	
	//set CSN to low
	P4->OUT &= ~2;
	//SETUP_AW
	address = 0x23;		//0b00100011;
	initData = 0x03;	//5 bytes address width
	SPI_Transmit_transfer(address);
	SPI_Transmit_transfer(initData);
	//set CSN to high
	P4->OUT |= 2;
	
	//read register
	uart0_put("Reading... SETUP_AW: ");
	read_Transmit_NRF_Register(0x03);
	
	
	
	//set CSN to low
	P4->OUT &= ~2;
	//SETUP_RETR
	address = 0x24;				//0b00100100;
	initData = 0x00;	//no retransmits and 0 delay
	SPI_Transmit_transfer(address);
	SPI_Transmit_transfer(initData);
	//set CSN to high
	P4->OUT |= 2;
	
	//read register
	uart0_put("Reading... SETUP_RETR: ");
	read_Transmit_NRF_Register(0x04);
	
	
	
	//set CSN to low
	P4->OUT &= ~2;
	//STATUS Reg.
	address = 0x27;
	initData = 0x7E; //clear interrupts
	SPI_Transmit_transfer(address);
	SPI_Transmit_transfer(initData);
	//set CSN to high
	P4->OUT |= 2;
	
	//read register
	uart0_put("Reading... STATUS: ");
	read_Transmit_NRF_Register(0x07);
}
	
/*******************
END TRANSMIT NRF FUNCTIONS
*******************/


void clear_Interrupts(void){
		uint8_t address;

		//Clear interrupt
		//set CSN to low
		P2->OUT &= ~8;
		//STATUS Reg.
		address = 0x27;		
		SPI_Recieve_transfer(address);
		SPI_Recieve_transfer(0x4E);		//clear RX_DR
		//set CSN to high
		P2->OUT |= 8;

		//FLUSH_TX
		//set CSN to low
		P4->OUT &= ~2;
		address = 0xE1;				
		SPI_Transmit_transfer(address);
		//set CSN to high
		P4->OUT |= 2;
		
		//Clear MAX_RT in TX
		//set CSN to low
		P4->OUT &= ~2;
		//STATUS reg.
		address = 0x27;				
		SPI_Transmit_transfer(address);
		SPI_Transmit_transfer(0x7E);		//write 1 to MAX_RT to clear for further comm.
		//set CSN to high
		P4->OUT |= 2;
}
