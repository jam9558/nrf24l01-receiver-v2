#include "msp.h"
#include "Common.h"
#include "SPI.h"



void delayMs(int n)
 {
		volatile int i, j;
		for (j = 0; j < n; j++){
			for (i = 750; i > 0; i--){
			} /* delay 1 ms */
		}
}

void Init_Recieve_NRF_SPI(void){
 EUSCI_B0->CTLW0 = 0x0001; /* disable UCB0 during config */

 /* clock phase/polarity:11, MSB first, 8-bit, master, 3-pin SPI,
 synchronous mode, use SMCLK as clock source */

 EUSCI_B0->CTLW0 = 0xA9C1; //This actually almost worked 0xA9C1; //0xC9C1;	//this allows for LSB first, 3-pin SPI //OLD -> 0xE9C1;
 EUSCI_B0->BRW = 1; /* 3 MHz / 1 = 3 MHz */	
 EUSCI_B0->CTLW0 &= ~0x0001; /* enable UCB0 after config */
	
	
 //Configure 1.5 and 1.6 as outputs	
 //P1->DIR |= (BIT5 | BIT6);
	
 P1->SEL0 |= 0xE0; /* P1.5, P1.6 P1.7 for UCB0CLK, UCB0SIMO, UCB0SOMI */
 P1->SEL1 &= ~0xE0;
	
	
 P2->DIR |= 8; /* P2.3 set as output for slave select */
 P2->OUT |= 8; /* slave select idle high */
	
 //CE pin
 P3->SEL0 &= ~1;
 P3->SEL1 &= ~1;
 P3->DIR |= 1;
}

void Init_Transmit_NRF_SPI(void){
 //using EUSCI_B2 for Transmit bc recieve using
 //EUSCI_B0.
	
 EUSCI_B2->CTLW0 = 0x0001;
	
 EUSCI_B2->CTLW0 = 0xA9C1; //This actually almost worked 0xA9C1; //0xC9C1;	//this allows for LSB first, 3-pin SPI //OLD -> 0xE9C1;
 EUSCI_B2->BRW = 1; /* 3 MHz / 1 = 3 MHz */	
 EUSCI_B2->CTLW0 &= ~0x0001; /* enable UCB2 after config */
		
 //3.5 = UCB2CLK, 3.6 = MOSI, 3.7 = MISO
 P3->SEL0 |= 0xE0;
 P3->SEL1 &= ~0xE0;
	
 P4->DIR |= 2; /* P4.1 set as output for slave select */
 P4->OUT |= 2; /* slave select idle high */
	
	
 //CE pin = P4.6
 P4->SEL0 &= ~64;
 P4->SEL1 &= ~64;
 P4->DIR |= 64;
 
}

void SPI_Recieve_transfer(uint8_t d){
	
	while(!(EUSCI_B0->IFG & 2));
	EUSCI_B0->TXBUF = d;
	
	
	while(EUSCI_B0->STATW & 1);
	
	delayMs(50);
}

uint16_t SPI_Recieve_recieve(void){
	uint16_t recieve;
	
	
	while(!(EUSCI_B0->IFG & 1)); //wait for recieve buffer to be full (8 bits)
	recieve = EUSCI_B0->RXBUF;   //get the data
	while(EUSCI_B0->STATW & 1);  //wait for recieve to be done

	
	return recieve;
}

void SPI_Transmit_transfer(uint8_t d){
	
	while(!(EUSCI_B2->IFG & 2));
	EUSCI_B2->TXBUF = d;
	
	
	while(EUSCI_B2->STATW & 1);
	
	delayMs(50);
}

uint16_t SPI_Transmit_recieve(void){
	uint16_t recieve;
	
	
	while(!(EUSCI_B2->IFG & 1)); //wait for recieve buffer to be full (8 bits)
	recieve = EUSCI_B2->RXBUF;   //get the data
	while(EUSCI_B2->STATW & 1);  //wait for recieve to be done

	
	return recieve;
}
